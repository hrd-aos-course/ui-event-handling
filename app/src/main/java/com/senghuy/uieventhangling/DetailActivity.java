package com.senghuy.uieventhangling;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    TextView textTitle, textWebsite,textInfo, textROption, textJob;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        textTitle = findViewById(R.id.textTitle);
        textJob = findViewById(R.id.textOption);
        textWebsite = findViewById(R.id.textWebsite);
        textInfo = findViewById(R.id.textInfo);
        textROption = findViewById(R.id.textROption);

        Intent intent = getIntent();
        String title = intent.getStringExtra("Title");
        String job = intent.getStringExtra("Job");
        String eWebsite = intent.getStringExtra("eWebsite");
        String eInfo = intent.getStringExtra("eInfo");
        String radioOption = intent.getStringExtra("Option");

        textTitle.setText(title);
        textJob.setText(job);
        textWebsite.setText(eWebsite);
        textInfo.setText(eInfo);
        textROption.setText(radioOption);

    }
}