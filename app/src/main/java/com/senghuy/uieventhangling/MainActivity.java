package com.senghuy.uieventhangling;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    Profile profile = new Profile();

    Bitmap bmp;
    private RadioGroup radioGroup;
    RadioButton radioButton;

    // Declare variable to define ID from XML
    Button bSelectImage, bSave, bCancel;
    EditText eTitle, eWebsite, eInfo;
    RadioButton rWebsite, rFacebook;
    ImageView iPreviewImage;
    private Spinner spinner;

    // constant to compare the activity result code
    int SELECT_PICTURE = 200;

    // Declared array value on Spinner Item Selected
    String []jobs = {"Actor", "Teacher", "Developer", "Footballer"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // register the UI widgets with their appropriate IDs
        bSelectImage = findViewById(R.id.btnBrowse);
        iPreviewImage = findViewById(R.id.imageView);

        bSelectImage.setOnClickListener(view -> {
            onImageChooser();
        });

        spinner = (Spinner) findViewById(R.id.sOption);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        // Spinner method
        onSpinnerItemSelected();

        // Pass value to DetailActivity
        onClickListenerSaved();

    }

    private void onClickListenerSaved() {

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        // Get value from editText into object
        eTitle = findViewById(R.id.eTitle);
        eWebsite = findViewById(R.id.eWebsite);
        rWebsite = findViewById(R.id.rWebsite);
        rFacebook = findViewById(R.id.rFacebook);
        eInfo = findViewById(R.id.eInfo);
        iPreviewImage = findViewById(R.id.imageView);

        bSave = findViewById(R.id.bSave);

        // Event onClick pass value to DetailActivity
        bSave.setOnClickListener(view -> {

            // get selected radio button from radioGroup
            int selectedId = radioGroup.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            radioButton = (RadioButton) findViewById(selectedId);
            Log.d("TAG", "onClickListenerSaved: " + radioButton.getText());

            //get value from edittext into object
            profile.setTitle(eTitle.getText().toString());
            profile.setWebsite(eWebsite.getText().toString());
            profile.setInfo(eInfo.getText().toString());
            profile.setRadioOption(radioButton.getText().toString());

            //validate email
            String email = eInfo.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            if (profile.title.equals("")) {
                Toast.makeText(this, "Please enter your Title! ", Toast.LENGTH_SHORT).show();
            }else if (profile.website.equals("")){
                Toast.makeText(this, "Please enter an Website! ", Toast.LENGTH_SHORT).show();
            }else if (!email.matches(emailPattern)){
                Toast.makeText(this, "Invalid Email! ", Toast.LENGTH_SHORT).show();
            }else {
                // Use Intent to pass values to DetailActivity
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("Title", profile.title);
                intent.putExtra("eWebsite", profile.website);
                intent.putExtra("eInfo", profile.info);
                intent.putExtra("Job", profile.oJobs);
                intent.putExtra("Option", profile.radioOption);
                startActivity(intent);
            }
        });
    }

    // Declared spinner method
    private void onSpinnerItemSelected() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Log.d("TAG", "onItemClick: " + jobs[position]);
                Toast.makeText(getApplicationContext(), jobs[position], Toast.LENGTH_SHORT).show();
                profile.setoJobs(jobs[position]);
                Log.d("Profile", "onItemSelected: " + profile.oJobs);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d("TAG", "onItemClick: Nothing to do");
            }
        });
    }

    // this function is triggered when the Select Image Button is clicked
    private void onImageChooser() {

        iPreviewImage = findViewById(R.id.imageView);
        profile.setImage(iPreviewImage.toString());

        // create an instance of the intent of the type image
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

//        // pass the constant to compare it with the returned requestCode
        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            // compare the resultCode with the SELECT_PICTURE constant
            if (requestCode == SELECT_PICTURE) {
                // Get the url of the image from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // update the preview image in the layout
                    iPreviewImage.setImageURI(selectedImageUri);
                }
            }
        }
    }

}