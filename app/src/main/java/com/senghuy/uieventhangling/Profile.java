package com.senghuy.uieventhangling;

import android.graphics.Bitmap;

public class Profile {

    String title;
    String rWebsite;
    String rFacebook;
    String info;
    String website;
    String oJobs;
    String radioOption;

    Bitmap bitmap;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String image;

    public String getRadioOption() {
        return radioOption;
    }

    public void setRadioOption(String radioOption) {
        this.radioOption = radioOption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getrWebsite() {
        return rWebsite;
    }

    public void setrWebsite(String rWebsite) {
        this.rWebsite = rWebsite;
    }

    public String getrFacebook() {
        return rFacebook;
    }

    public void setrFacebook(String rFacebook) {
        this.rFacebook = rFacebook;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getoJobs() {
        return oJobs;
    }

    public void setoJobs(String oJobs) {
        this.oJobs = oJobs;
    }
}
